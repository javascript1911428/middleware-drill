const express = require("express");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const app = express();
const port = 7000;
const customLogger = require("./logger.js");

app.use(customLogger);

app.get("/logs", (req, res) => {
    fs.readFile("custom.log", "utf-8", (err, data) => {
        if (err) {
            console.log("Error:", err);
            res.status(500).send("Internal server error");
        } else {
            const splitData = data.split("\n").join("\n")
            res.send(splitData);
        }
    });
});

app.get("/html", (req, res) => {
    fs.readFile("/home/rama/expressjsdrill/public/index.html", "utf8", (err, htmldata) => {
        if (err) {
            console.log("Error:", err)
        } else {
            res.send(htmldata);
        }
    })

});

app.get("/json", (req, res) => {
    fs.readFile("/home/rama/expressjsdrill/public/data.json", "utf8", (err, jsondata) => {
        if (err) {
            console.log("Error:", err)
        } else {
            res.json(jsondata);
        }
    })

});

app.get("/uuid", (req, res) => {
    const newUuid = uuidv4();
    const uuidObject = {
        uuid: newUuid
    };
    res.json(uuidObject);
});

app.get("/status/:statusCode", (req, res) => {
    const statusCode = parseInt(req.params.statusCode);
    res.status(statusCode).send(`Response with status code: ${statusCode}`);
});

app.get("/delay/:delayInSeconds", (req, res) => {
    const delayInSeconds = parseInt(req.params.delayInSeconds);
    if (isNaN(delayInSeconds) || delayInSeconds <= 0) {
        return res.status(400).send("Invalid delay_in_seconds")
    }

    setTimeout(() => {
        res.status(200).send(`response delayed by ${delayInSeconds} seconds`)
    }, delayInSeconds * 1000);
});

app.listen(port, (err) => {
    if (err) {
        console.log("Error:", err)
    }
    console.log(`Server is running on http://localhost:${port}`);
});