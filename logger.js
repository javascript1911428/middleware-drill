const fs = require("fs");

function customLogger(req, res, next) {
    const date = new Date().toISOString();
    const customLogMessage = ` ${date} - ${req.method} ${req.url} \n`;
    fs.appendFile("custom.log", customLogMessage, (err) => {
        if (err) {
            console.log("Error:", err)
        }
        else { next() }
    });

}

module.exports = customLogger;